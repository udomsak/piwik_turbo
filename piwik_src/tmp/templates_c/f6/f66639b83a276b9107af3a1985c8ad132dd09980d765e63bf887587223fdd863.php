<?php

/* @CoreHome/_siteSelectHeader.twig */
class __TwigTemplate_d17d4c6598fee0ba6dba09e14c9d1608f6352d89ddab94f376ada790757c3177 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"top_bar_sites_selector piwikTopControl\">
    <div piwik-siteselector show-selected-site=\"true\" class=\"sites_autocomplete\"></div>
</div>";
    }

    public function getTemplateName()
    {
        return "@CoreHome/_siteSelectHeader.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* <div class="top_bar_sites_selector piwikTopControl">*/
/*     <div piwik-siteselector show-selected-site="true" class="sites_autocomplete"></div>*/
/* </div>*/
