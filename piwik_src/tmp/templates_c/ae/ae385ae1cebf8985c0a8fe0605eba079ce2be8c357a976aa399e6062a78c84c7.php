<?php

/* _sparklineFooter.twig */
class __TwigTemplate_6a457714c561553bbee05e77ded4a8d2d615eca2f1b4ab13709f5fc5c8a9b288 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<script type=\"text/javascript\">
    \$(function () {
        initializeSparklines();
    });
</script>
";
    }

    public function getTemplateName()
    {
        return "_sparklineFooter.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* <script type="text/javascript">*/
/*     $(function () {*/
/*         initializeSparklines();*/
/*     });*/
/* </script>*/
/* */
